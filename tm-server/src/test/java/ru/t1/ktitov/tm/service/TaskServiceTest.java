package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.IProjectService;
import ru.t1.ktitov.tm.api.service.model.ITaskService;
import ru.t1.ktitov.tm.api.service.model.IUserService;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.service.model.ProjectService;
import ru.t1.ktitov.tm.service.model.UserService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Component
@Category(UnitCategory.class)
public final class TaskServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskService service;

    private void compareTasks(@NotNull final Task task1, @NotNull final Task task2) {
        Assert.assertEquals(task1.getId(), task2.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task1.getDescription(), task2.getDescription());
        Assert.assertEquals(task1.getStatus(), task2.getStatus());
        Assert.assertEquals(task1.getUser().getId(), task2.getUser().getId());
        Assert.assertEquals(task1.getCreated(), task2.getCreated());
    }

    private void compareTasks(
            @NotNull final List<Task> taskList1,
            @NotNull final List<Task> taskList2) {
        Assert.assertEquals(taskList1.size(), taskList2.size());
        for (int i = 0; i < taskList1.size(); i++) {
            compareTasks(taskList1.get(i), taskList2.get(i));
        }
    }

    @Before
    public void initData() {
        @NotNull final IUserService userService = new UserService();
        userService.add(USER1);
        userService.add(USER2);
        userService.add(ADMIN3);
        @NotNull final IProjectService projectService = new ProjectService();
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(USER2_PROJECT_LIST);
        projectService.add(ADMIN1_PROJECT_LIST);
    }

    @After
    public void tearDown() {
        @NotNull final IUserService userService = new UserService();
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN3.getId());
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        compareTasks(USER1_TASK1, service.findAll().get(0));
        compareTasks(USER2_TASK1, service.findAll().get(1));
    }

    @Test
    public void addByUserId() {
        service.add(USER1.getId(), USER1_TASK1);
        compareTasks(USER1_TASK1, service.findAll().get(0));
        Assert.assertEquals(USER1.getId(), service.findAll().get(0).getUser().getId());
    }

    @Test
    public void addList() {
        service.add(USER1_TASK_LIST);
        Assert.assertEquals(3, service.findAll().size());
        compareTasks(USER1_TASK1, service.findAll().get(0));
        compareTasks(USER1_TASK2, service.findAll().get(1));
        compareTasks(USER1_TASK3, service.findAll().get(2));
    }

    @Test
    public void setList() {
        service.add(USER1_TASK_LIST);
        service.set(USER2_TASK_LIST);
        compareTasks(USER2_TASK1, service.findAll().get(0));
    }

    @Test
    public void clear() {
        service.add(USER1_TASK_LIST);
        Assert.assertEquals(3, service.findAll().size());
        service.clear();
        Assert.assertEquals(0, service.findAll().size());
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.clear(USER1.getId());
        Assert.assertEquals(1, service.findAll().size());
        compareTasks(USER2_TASK1, service.findAll().get(0));
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.add(ADMIN1_TASK_LIST);
        compareTasks(USER1_TASK_LIST, service.findAll(USER1.getId()));
        compareTasks(USER2_TASK_LIST, service.findAll(USER2.getId()));
        compareTasks(ADMIN1_TASK_LIST, service.findAll(ADMIN3.getId()));
    }

    @Test
    public void existsById() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        Assert.assertTrue(service.existsById(USER1_TASK1.getId()));
        Assert.assertTrue(service.existsById(USER2_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER1_TASK2.getId()));
        Assert.assertTrue(service.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        compareTasks(USER1_TASK1, service.findOneById(USER1_TASK1.getId()));
        compareTasks(USER2_TASK1, service.findOneById(USER2_TASK1.getId()));
        compareTasks(USER1_TASK1, service.findOneById(USER1.getId(), USER1_TASK1.getId()));
        thrown.expect(EntityNotFoundException.class);
        compareTasks(USER1_TASK2, service.findOneById(USER1_TASK2.getId()));
        compareTasks(USER2_TASK1, service.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void remove() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        Assert.assertEquals(4, service.findAll().size());
        service.remove(USER1_TASK1);
        Assert.assertEquals(3, service.findAll().size());
        service.removeById(USER1_TASK2.getId());
        Assert.assertEquals(2, service.findAll().size());
        compareTasks(USER1_TASK3, service.findAll().get(0));
        compareTasks(USER2_TASK1, service.findAll().get(1));
        service.clear();
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    public void removeByUserId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        Assert.assertEquals(4, service.findAll().size());
        service.remove(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(3, service.findAll().size());
        service.removeById(USER1.getId(), USER1_TASK2.getId());
        Assert.assertEquals(2, service.findAll().size());
        thrown.expect(EntityNotFoundException.class);
        service.remove(USER2.getId(), USER1_TASK3);
        Assert.assertEquals(2, service.findAll().size());
        compareTasks(USER1_TASK3, service.findAll().get(0));
        compareTasks(USER2_TASK1, service.findAll().get(1));
    }

    @Test
    public void create() {
        service.create(USER2.getId(), "task-2", "description of task 2");
        Assert.assertEquals(1, service.findAll().size());
        Assert.assertEquals("task-2", service.findAll().get(0).getName());
        Assert.assertEquals("description of task 2", service.findAll().get(0).getDescription());
        Assert.assertEquals(Status.NOT_STARTED, service.findAll().get(0).getStatus());
    }

    @Test
    public void findAllByProjectId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.add(ADMIN1_TASK_LIST);
        compareTasks(USER1_TASK_LIST, service.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void updateById() {
        @NotNull final Task task = service.create(USER1.getId(),
                "task-4", "description of task 4");
        service.updateById(USER1.getId(), task.getId(),
                "upd-task-4", "upd description of task 4");
        @NotNull final Task foundTask = service.findOneById(task.getId());
        Assert.assertEquals("upd-task-4", foundTask.getName());
        Assert.assertEquals("upd description of task 4", foundTask.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        service.add(USER1_TASK1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK1.getStatus());
        service.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        @NotNull final Task foundTask = service.findOneById(USER1_PROJECT1.getId());
        Assert.assertEquals(Status.IN_PROGRESS, foundTask.getStatus());
    }

}
