package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;

@Component
@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Test
    public void getServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

    @Test
    public void getPasswordIteration() {
        Assert.assertNotNull(propertyService.getPasswordIteration());
    }

    @Test
    public void getPasswordSecret() {
        Assert.assertNotNull(propertyService.getPasswordSecret());
    }

}
