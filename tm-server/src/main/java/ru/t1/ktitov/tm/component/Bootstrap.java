package ru.t1.ktitov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.service.*;
import ru.t1.ktitov.tm.api.service.dto.IUserDtoService;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.endpoint.*;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] abstractEndpoints;

    @NotNull
    @Autowired
    private Backup backup;

    private void registry(@NotNull final AbstractEndpoint[] endpoints) {
        if (endpoints == null) return;
        for (AbstractEndpoint endpoint : endpoints)
            registry(endpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

/*    @Autowired
    private IUserDtoService userService;

    private void initDemoData() {
        @NotNull final UserDTO test = userService.create("test", "test", "test@test.ru");
        @NotNull final UserDTO user = userService.create("user", "user", "user@user.ru");
        @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);
    }*/

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
    }

    public void run(@Nullable final String[] args) {
        initPID();
//        initDemoData();
        registry(abstractEndpoints);
        loggerService.info("** WELCOME TO TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
//        backup.start();
    }

}
