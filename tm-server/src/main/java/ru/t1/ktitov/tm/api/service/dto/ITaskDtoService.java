package ru.t1.ktitov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDTO> {

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

}
