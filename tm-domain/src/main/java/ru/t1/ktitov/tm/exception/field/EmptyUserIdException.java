package ru.t1.ktitov.tm.exception.field;

public class EmptyUserIdException extends AbstractFieldException {

    public EmptyUserIdException() {
        super("Error! UserId is empty.");
    }

}
