package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataBase64LoadRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public final class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    public static final String DESCRIPTION = "Load data from base64 file";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64LoadListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        domainEndpoint.loadDataBase64(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
