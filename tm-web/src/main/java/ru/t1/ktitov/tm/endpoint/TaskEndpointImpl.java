package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.repository.TaskRepository;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public final class TaskEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @PostMapping("/create")
    public void create() {
        taskRepository.create();
    }

    @Override
    @PutMapping("/save")
    public void save(@RequestBody final Task task) {
        taskRepository.save(task);
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@RequestBody final Task task) {
        taskRepository.removeById(task.getId());
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") final String id) {
        taskRepository.removeById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @GetMapping("/find/{id}")
    public Task findById(@PathVariable("id") final String id) {
        return taskRepository.findById(id);
    }

}
