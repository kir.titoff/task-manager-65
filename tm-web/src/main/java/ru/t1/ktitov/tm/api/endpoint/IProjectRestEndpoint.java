package ru.t1.ktitov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.ktitov.tm.model.Project;

import java.util.Collection;

public interface IProjectRestEndpoint {

    void create();

    void save(@RequestBody Project project);

    void delete(@RequestBody Project project);

    void deleteById(@PathVariable("id") String id);

    Collection<Project> findAll();

    Project findById(@PathVariable("id") String id);

}
