package ru.t1.ktitov.tm.api.endpoint;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.ktitov.tm.model.Task;

import java.util.Collection;

public interface ITaskRestEndpoint {

    void create();

    void save(@RequestBody Task task);

    void delete(@RequestBody Task task);

    void deleteById(@PathVariable("id") String id);

    Collection<Task> findAll();

    Task findById(@PathVariable("id") String id);

}
